const listsCollection = document.getElementsByClassName("column");
const leftList = document.querySelector("#left-list");
const centerList = document.querySelector("#center-list");
const rightList = document.querySelector("#right-list");
const textarea = document.querySelector("#content");
const counter = document.querySelector("#current-count");
const form = document.forms.namedItem("form");
const dialog = document.querySelector("#delete-dialog");
const confirmBtn = document.querySelector("#confirmBtn");
const cancelBtn = document.querySelector("#cancelBtn");
const colorsArray = ["lightgreen", "lightblue", "lightpink", "#fec"];



// utileisé pour l'id de chaque post-it
let postItNextId = 0;

// id du post-it à supprimer
let toBeDeleted = -1;


/**
 * Comptage de char dans le textarea
 */
textarea.addEventListener("input", (event) => {
    let numOfEnteredChars = textarea.value.length;
    counter.innerHTML = numOfEnteredChars;
});

/**
 * Fonction de stockage des données
 */

// Met à jour les données de chaque listes dans le localstorage
let updateListsData = function () {
    const leftListData = leftList.innerHTML;
    const centerListData = centerList.innerHTML;
    const rightListData = rightList.innerHTML;

    localStorage.setItem("leftList", leftListData);
    localStorage.setItem("centerList", centerListData);
    localStorage.setItem("rightList", rightListData);
};

// Va chercher les données dans le localstorage
let fetchListData = function () {
    const leftListData = localStorage.getItem("leftList");
    const centerListData = localStorage.getItem("centerList");
    const rightListData = localStorage.getItem("rightList");

    leftList.innerHTML = leftListData;
    centerList.innerHTML = centerListData;
    rightList.innerHTML = rightListData;

    let postIts = document.getElementsByClassName("post-it");

    Array.from(postIts).forEach(element => {
        // on rend chaque post-it dragable
        element.addEventListener("dragstart", (ev) => drag(ev));
        // on rend actif chaque bouton poubelle
        let button = element.children[0].children[2];
        button.id = "button-" + element.id.replace(/[^0-9]/g, "");
        button.addEventListener("click", (ev) => openDialog(ev));
        // le compteur de post-it prend la valeur de l'id le plus grand
        postItNextId = Math.max(postItNextId, parseInt(element.id.replace(/[^0-9]/g, "")));
    });
}

/**
 * Fonctions du drag and drop
 */
function allowDrop(ev) {
    ev.preventDefault();
    ev.dataTransfer.dropEffect = "move";
    ev.target.style.backgroundColor = "lightgrey";
};

function exitDrop(ev) {
    ev.preventDefault();
    ev.target.style.backgroundColor = "whitesmoke";
};

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
};

function drop(ev) {
    ev.preventDefault();
    ev.target.style.backgroundColor = "whitesmoke";
    const data = ev.dataTransfer.getData("text");
    // la cible de l'event est la colonne, on veut ajouter le post-it dans la liste da la colonne
    const list = ev.target.children[1];
    const postIt = document.getElementById(data);
    list.appendChild(postIt);
    postIt.addEventListener("dragstart", (ev) => drag(ev));

    updateListsData();
};

/**
 *  Rend chaque colonnes (et pas leurs enfants) en zone de drop
 */
Array.from(listsCollection).forEach(element => {
    element.addEventListener("dragover", ev => {
        if (element !== ev.target) return;
        allowDrop(ev);
    });

    element.addEventListener("dragleave", ev => {
        if (element !== ev.target) return;
        exitDrop(ev);
    });
    element.addEventListener("drop", ev => {
        if (element !== ev.target) return;
        drop(ev);
    });
});


let deletePostIt = function () {
    const postIt = document.getElementById("post-it-" + toBeDeleted);
    postIt.remove();
    toBeDeleted = -1;
    updateListsData();
    dialog.close();
};

let openDialog = function (ev) {
    toBeDeleted = ev.target.parentElement.id.replace(/[^0-9]/g, "");
    dialog.showModal();
};

let createPostIt = function (ev) {

    ev.preventDefault();
    counter.innerHTML = 0;
    postItNextId += 1;

    //récupération des information du formulaire
    let formData = new FormData(ev.target);

    let jsonData = Object.fromEntries(formData.entries());

    ev.target.reset();

    // création du post-it
    let postIt = document.createElement("li");
    postIt.innerHTML = "<a href=\"#\" draggable=\"false\"><h3 contenteditable=\"true\">" + jsonData.name + "</h3><p contenteditable=\"true\">" + jsonData.content + "</p>" +
        "<button class=\"delete\"><img src=\"img/deleteIcon.svg\" alt=\"delete\" width=\"20px\" height=\"20px\" /></button></a>";
    postIt.id = "post-it-" + postItNextId;
    postIt.className = "post-it";
    postIt.setAttribute("draggable", true);
    postIt.style.transform = "rotate(" + Math.floor(10 * (Math.random() - 0.5)) + "deg)";
    postIt.addEventListener("dragstart", (ev) => drag(ev));

    const link = postIt.children[0];

    link.style.backgroundColor = colorsArray[Math.floor(Math.random() * colorsArray.length)];
    leftList.append(postIt);

    const button = link.children[2];
    button.id = "button-" + postItNextId;

    button.addEventListener("click", (ev) => openDialog(ev));
    updateListsData();
};

window.addEventListener('DOMContentLoaded', event => {
    form.addEventListener("submit", (ev) => createPostIt(ev));

    cancelBtn.addEventListener("click", () => dialog.close());
    confirmBtn.addEventListener("click", () => deletePostIt());
    fetchListData();
    // localStorage.clear();
});